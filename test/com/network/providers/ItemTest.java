package com.network.providers;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	
	Provider test_item3;

	@Before
	public void setUp() throws Exception {
		
		test_item3 = new Provider("T-Mobile",70.00,"https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/T-Mobile_Logo.svg/2000px-T-Mobile_Logo.svg.png");
	}

	@Test
	public void testitem3desc() {
		
		String expectedDescription = test_item3.getDescription();
		String actualDescription = "T-Mobile";
		assertEquals(expectedDescription, actualDescription);
		
	}
	
	@Test
	public void testitem3price() {
		
		double expectedPrice = test_item3.getPrice();
		double actualPrice = 70.00;
		assertEquals(expectedPrice, actualPrice,0.001);
		
	}
}
