package com.network.providers;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.ws.Endpoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventoryServiceTest {
	
	private List<Provider> providers;
	
	@Before
	public void setUp() throws Exception {
		providers = new NetworkService().getAllItems();
	}

	@Test
	public void testSize() {
		int actual = providers.size();
		int expected = 4;
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testdescription() {
		
		String actual = providers.get(0).getDescription();
		String expected = "Sprint Corporation";
		
		assertEquals(expected, actual);
	}
	@Test
	public void testprice() {
		
		double actual = providers.get(0).getPrice();
		double expected = 50.00;
		
		assertEquals(expected, actual,0.001);
	}
}
