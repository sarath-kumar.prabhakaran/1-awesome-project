package com.network.providers;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventoryTest {
	
	private Provider actual_item1;
	private Provider actual_item2;
		
	private List<Provider> itemTest=null;
	
	@Before
	public void Start() {
		
		actual_item1 = new Provider("Sprint Corporation",50.00,"https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Logo_of_Sprint_Nextel.svg/2000px-Logo_of_Sprint_Nextel.svg.png");
		actual_item2 = new Provider("T-Mobile",70.00,"https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/T-Mobile_Logo.svg/2000px-T-Mobile_Logo.svg.png");
			
		itemTest = new ArrayList<Provider>();
		
		itemTest.add(actual_item1);
		itemTest.add(actual_item2);
	}

	@Test
	public void testforItems() {
		
		Network TestNetwork = new Network();
		TestNetwork.setItems(itemTest);

		assertEquals(2, TestNetwork.getItems().size());		
	}

}
