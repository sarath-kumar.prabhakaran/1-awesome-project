package com.network.providers;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class NetworkService {
	
	private JaxbToJava productGenerated;

	@WebMethod
	public List<Provider> getAllItems () {
		productGenerated = new JaxbToJava();

		return productGenerated.Convert();
	}
}
