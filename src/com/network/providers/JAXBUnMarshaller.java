package com.network.providers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class JAXBUnMarshaller {

 public static List<Provider> unmarshal(File fileArg) throws IOException, JAXBException{
		 
		 Network networks = new Network();
		 JAXBContext context = JAXBContext.newInstance(Network.class);
		 Unmarshaller um = context.createUnmarshaller();
		 networks = (Network) um.unmarshal(fileArg);
		 
		 return networks.getItems();
	 }
}
