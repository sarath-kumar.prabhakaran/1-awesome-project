package com.network.providers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

public class JaxbToJava {

	public List<Provider> Convert(){   
		List<Provider> ProductList=null;
		try {
			ProductList = JAXBUnMarshaller.unmarshal(new File("network.xml"));
		} catch (IOException | JAXBException e) {
			e.printStackTrace();
		}
		return ProductList;
	}
	
	

}
