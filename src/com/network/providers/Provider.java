package com.network.providers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"description", "price","image"})
@XmlRootElement
//@XmlAccessorType(XmlAccessType.FIELD)
public class Provider {

	private String description;
	private double price;
	private String image;
	
	public Provider() {

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Provider(String description, double price,String image) {
		super();
		this.setDescription(description);
		this.setPrice(price);
		this.setImage(image);
	}


	
	
	

}
